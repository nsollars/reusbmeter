#include "btconnect.h"
#include "ui_btconnect.h"
#include "mainwindow.h"
#include<QListWidget>

BTConnect::BTConnect(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BTConnect)
{
    ui->setupUi(this);
    ui->btDeviceList->clear();

    connect(Bluetooth::agent, SIGNAL(deviceDiscovered(QBluetoothDeviceInfo)),
            this, SLOT(deviceDiscovered(QBluetoothDeviceInfo)));
    Bluetooth::agent->start();
}

BTConnect::~BTConnect()
{
    delete ui;
}

void BTConnect::on_findDevice_clicked()
{
    ui->btDeviceList->clear();
    Bluetooth::agent->stop();
    Bluetooth::agent->start();

    if(!Bluetooth::agent->isActive()) {
        QMessageBox msgBox(this);
        msgBox.setWindowTitle("Bluetooth is disabled");
        msgBox.setText("Please enable Bluetooth or enter device passcode");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
    }
}

void BTConnect::deviceDiscovered(const QBluetoothDeviceInfo &device) {
    ui->btDeviceList->addItem("(" + device.name() + ") " + device.address().toString());
}

void BTConnect::on_btDeviceList_itemClicked(QListWidgetItem *item)
{
    /*
     *  Connect to selected device ( in the listWindow ) then close on complete.
     */

    QString string = item->text();
    string = string.mid(string.lastIndexOf(" ")+1, string.length()-1);
    static const QString serviceUuid(QStringLiteral("00001101-0000-1000-8000-00805F9B34FB"));
    Bluetooth::socket->connectToService(QBluetoothAddress(string), QBluetoothUuid(serviceUuid), QIODevice::ReadWrite);

    this->close();
}

void BTConnect::on_cancelButton_clicked()
{
    this->close();
}
