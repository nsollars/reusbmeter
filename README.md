# ReUSBMeter

![Screenshot](images/Ruideng_USB_Devices.png)

An absolute re-implementation application that supports Ruideng USB Meter devices,

    -   UM34c
    -   UM25c
    -   UM24c

Using the bluetooth protocol.  It is written in C++ leveraging the QT widget toolkit and tools.  This application is in active development to implement basic monitoring in a timely fashion.

What currently works,

    -   Bluetooth connect/disconnect ( with LED visual )
    -   USB Device Screen control ( Backlight, Next / Previous Screens, Rotate Screen )

    Todo: Adding of charts and background task/thread for data retrieval


Thanks to,

    -   RuidengMeter software project    Application reference
    -   Sigrok                           Protocol reverse engineering reference
