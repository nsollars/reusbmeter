#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothSocket>
#include <QtCharts>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

/*
 * UI Items
 */

private slots:

    void on_actionQuit_triggered();
    void on_actionAbout_triggered();
    void isConnected();
    void isDisconnected();
    void on_devicecon_clicked();
    void on_deviceDiscon_clicked();
    void on_lightSlider_sliderMoved(int value);
    void on_nextButton_clicked();
    void on_previousButton_clicked();
    void on_rotateButton_clicked();
//  void doDataFetchRead();
//  void updateVoltAmpChart();
//  void updateWattChart();

private:
    Ui::MainWindow *ui;
};

class Bluetooth
{
public:
    static QBluetoothDeviceDiscoveryAgent *agent;
    static QBluetoothSocket *socket;
};

#endif // MAINWINDOW_H
