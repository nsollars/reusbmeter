#ifndef RUIDENGPROTOCOL_H
#define RUIDENGPROTOCOL_H

/*
 *  Ruideng Bluetooth protocol
 *
 *  These commands control the actual device and configure various
 *  options
 */

/*
 *  LCD Screen / Menu
 */

#define     nextScreen              0xf1               /*  Device Control  */
#define     lastScreen              0xf3               /*  Device Control  */
#define     rotateScreen            0xf2               /*  Device Control  */

/*
 *  Data
 */

#define     dataRequest             0xf0               /*  Request new datadump    */

#endif // RUIDENGPROTOCOL_H
